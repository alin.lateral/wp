self.addEventListener('push', (e) => {
    let body;

    if (e.data) {
      body = e.data.text();
    } else {
      body = 'Push message no payload';
    }
  
    let options = {
      body: body,
      icon: 'https://images.unsplash.com/photo-1621343498292-eb469c743b25?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2134&q=80',
      vibrate: [100, 50, 100],
      data: {
        dateOfArrival: Date.now(),
        primaryKey: 1
      },
      actions: [
        {action: 'explore', title: 'View the thing' },
        {action: 'close', title: 'I don\'t want any of this'},
      ]
    };
    e.waitUntil(
      self.registration.showNotification('Push Notification', options)
    );
});